package com.eduardomaia.batalhanaval.objects;

import java.util.ArrayList;

/**
 * Created by Eduardo Maia on 22/07/17.
 */

public class Grid {

    private ArrayList<Integer> gridValues;
    private ArrayList<Integer> gridIds;

    public Grid() {
        gridValues = new ArrayList<>();
        gridIds = new ArrayList<>();
    }

    //Check if ship can be placed on grid
    //Grid values are from:
    //11 to 18
    //21 to 28
    //31 to 38
    //41 to 48
    //51 to 58
    //61 to 68
    //71 to 78
    //81 to 88

    public boolean checkShipOnGrid(int id, ArrayList<String> rowcol) {
        if(gridValues.size() > 0) {
            for (int i = 0; i < rowcol.size(); i++) {
                for (int j = 0; j < gridValues.size(); j++) {
                    if (gridValues.get(j) == Integer.parseInt(rowcol.get(i)) && id != gridIds.get(j))
                        return false;
                }
            }
        }

        for (int i = 0; i < rowcol.size(); i++) {
            String[] getRowAndCol = rowcol.get(i).split("");

            if(Integer.parseInt(getRowAndCol[1]) > 8 || Integer.parseInt(getRowAndCol[2]) > 8)
                return false;
        }

        return true;
    }

    //Save the position filled

    public void saveShipOnGrid(int id, int pos) {
        gridValues.add(pos);
        gridIds.add(id);
    }

    //Erase the position changed

    public void eraseShipOnGrid(int id)
    {
        for (int i = 0; i < gridValues.size(); i++) {
            if (gridValues.get(i) == id) {
                gridValues.remove(i);
                gridIds.remove(i);
                    break;
            }
        }
    }
}
