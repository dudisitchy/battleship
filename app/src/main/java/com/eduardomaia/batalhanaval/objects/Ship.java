package com.eduardomaia.batalhanaval.objects;

import java.io.Serializable;

/**
 * Created by Eduardo Maia on 22/07/17.
 */

public class Ship implements Serializable {

    private int shipSize;
    private boolean onGrid;
    private int color;

    public Ship(int size, int color, boolean onGrid) {
        this.shipSize = size;
        this.color = color;
        this.onGrid = onGrid;
    }

    public int getShipSize() {
        return shipSize;
    }

    public void setShipSize(int shipSize) {
        this.shipSize = shipSize;
    }

    public int getColor() {
        return color;
    }

    public void setColor(int color) {
        this.color = color;
    }

    public boolean isOnGrid() {
        return onGrid;
    }

    public void setOnGrid(boolean onGrid) {
        this.onGrid = onGrid;
    }
}
