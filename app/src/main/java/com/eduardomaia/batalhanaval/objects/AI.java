package com.eduardomaia.batalhanaval.objects;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Random;

/**
 * Created by Eduardo Maia on 23/07/17.
 */

public class AI {

    private int[] ship_lengths = {5, 4, 3, 3, 2, 2};
    private int limitX = 8;
    private int limitY = 8;
    private int totalShips = 6;
    private Random rand = new Random();
    private String[] mapGrid;

    //Map all the positions of grid on construct

    public AI() {
        mapGrid = new String[limitX * limitY];
        int count = 0;

        for(int i = 1; i <= limitX; i++) {
            for(int j = 1; j <= limitY; j++) {
                mapGrid[count] = i + "" + j;
                count++;
            }
        }
    }

    //Get map's random values and verify if ship length can be inserted
    //on grid without overlap another ship or overcome the grid

    public HashMap<String, Integer> randomizeShips()
    {
        HashMap<String, Integer> AIShips = new HashMap<>();
        int axis;
        String pivot;

        for(int i = 0; i < totalShips; i++) {
            axis = rand.nextInt(2);
            boolean valid = false;
            String[] candidates = new String[ship_lengths[i]];

            while(!valid) {
                pivot = mapGrid[rand.nextInt(limitX * limitY)];
                candidates = new String[ship_lengths[i]];

                if (!AIShips.containsKey(pivot)) {
                    int shipSize = ship_lengths[i];
                    String[] getRowAndCol = pivot.split("");
                    candidates[0] = pivot;
                    boolean canInsert = true;

                    for (int j = 1; j < shipSize; j++) {
                        if (axis == 0) {
                            if (AIShips.containsKey(getRowAndCol[1] + "" + (Integer.parseInt(getRowAndCol[2]) + j)) || (Integer.parseInt(getRowAndCol[2]) + j) > limitX) {
                                canInsert = false;
                                break;
                            } else {
                                candidates[j] = getRowAndCol[1] + "" + (Integer.parseInt(getRowAndCol[2]) + j);
                            }
                        }
                        else {
                            if (AIShips.containsKey((Integer.parseInt(getRowAndCol[1]) + j) + "" + getRowAndCol[2]) || (Integer.parseInt(getRowAndCol[1]) + j) > limitY) {
                                canInsert = false;
                                break;
                            } else {
                                candidates[j] = (Integer.parseInt(getRowAndCol[1]) + j) + "" + getRowAndCol[2];
                            }
                        }
                    }

                    if(canInsert) {
                        valid = true;
                    }
                }
            }

            for(int j = 0; j < candidates.length; j++) {
                AIShips.put(candidates[j], i);
            }
        }

        return AIShips;
    }

    //Choose random position to shoot without repeat

    public int play(ArrayList<Integer> listPlays)
    {
        boolean valid = false;
        String fire = "";

        while(!valid) {
            fire = mapGrid[rand.nextInt(limitX * limitY)];
            boolean exist = false;

            for (int i = 0; i < listPlays.size(); i++) {
                if (listPlays.get(i) == Integer.parseInt(fire)) {
                    exist = true;
                    break;
                }
            }

            if(!exist) {
                valid = true;
            }
        }

        return Integer.parseInt(fire);
    }
}
