package com.eduardomaia.batalhanaval.views;

import android.app.Activity;
import android.content.Intent;
import android.graphics.drawable.GradientDrawable;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.Toast;

import com.eduardomaia.batalhanaval.R;
import com.eduardomaia.batalhanaval.objects.AI;
import com.eduardomaia.batalhanaval.objects.Grid;
import com.eduardomaia.batalhanaval.objects.Ship;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Eduardo Maia on 22/07/17.
 */

public class SetupShipsActivity extends Activity {

    private ImageView ship1;
    private ImageView ship2;
    private ImageView ship3;
    private ImageView ship4;
    private ImageView ship5;
    private ImageView ship6;
    private Button rotate;
    private Button startGame;
    private ArrayList<ImageView> imageShips;
    private HashMap<String, Integer> imageGrid;
    private ArrayList<Ship> battleShips;
    private int selectedShip;
    private String orientation;
    private Grid grid;
    private HashMap<String, Integer> posShipOnGrid;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setup_ships);

        //Setup initial vars and widgets

        ship1 = (ImageView) findViewById(R.id.battleship1);
        ship2 = (ImageView) findViewById(R.id.battleship2);
        ship3 = (ImageView) findViewById(R.id.battleship3);
        ship4 = (ImageView) findViewById(R.id.battleship4);
        ship5 = (ImageView) findViewById(R.id.battleship5);
        ship6 = (ImageView) findViewById(R.id.battleship6);

        imageShips = new ArrayList<>();
        imageShips.add(ship1);
        imageShips.add(ship2);
        imageShips.add(ship3);
        imageShips.add(ship4);
        imageShips.add(ship5);
        imageShips.add(ship6);

        Ship battleship1 = new Ship(2, R.color.ship_1, false);
        Ship battleship2 = new Ship(3, R.color.ship_2, false);
        Ship battleship3 = new Ship(4, R.color.ship_3, false);
        Ship battleship4 = new Ship(3, R.color.ship_4, false);
        Ship battleship5 = new Ship(2, R.color.ship_5, false);
        Ship battleship6 = new Ship(5, R.color.ship_6, false);

        battleShips = new ArrayList<>();
        battleShips.add(battleship1);
        battleShips.add(battleship2);
        battleShips.add(battleship3);
        battleShips.add(battleship4);
        battleShips.add(battleship5);
        battleShips.add(battleship6);

        imageGrid = new HashMap<>();
        orientation = getResources().getString(R.string.horizontal);
        selectedShip = -1;

        rotate = (Button) findViewById(R.id.rotate);
        startGame = (Button) findViewById(R.id.start);

        grid = new Grid();
        posShipOnGrid = new HashMap<>();

        //Generates AI's battleship random positions

        AI ai = new AI();
        final HashMap<String, Integer> AIships = ai.randomizeShips();

        //Setup grid for user interaction

        TableLayout table = (TableLayout) findViewById(R.id.grid);

        for(int row = 0; row < table.getChildCount(); row++) {
            View view = table.getChildAt(row);

            if(view instanceof TableRow) {
                TableRow tableRow = (TableRow) table.getChildAt(row);

                for(int col = 0; col < tableRow.getChildCount(); col++) {
                    if (tableRow.getChildAt(col) instanceof ImageView) {
                        View cellView = tableRow.getChildAt(col);
                        cellView.setOnClickListener(new ImageViewClickListener(row, col));
                        imageGrid.put(row + "" + col, cellView.getId());
                    }
                }
            }
        }

        //Click on each ship

        ship1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectedShip = 0;
                setBackgroundColor(ship1);
            }
        });

        ship2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectedShip = 1;
                setBackgroundColor(ship2);
            }
        });

        ship3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectedShip = 2;
                setBackgroundColor(ship3);
            }
        });

        ship4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectedShip = 3;
                setBackgroundColor(ship4);
            }
        });

        ship5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectedShip = 4;
                setBackgroundColor(ship5);
            }
        });

        ship6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectedShip = 5;
                setBackgroundColor(ship6);
            }
        });

        //Rotate the ship on grid

        rotate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(orientation.equals(getResources().getString(R.string.horizontal)))
                    orientation = getResources().getString(R.string.vertical);
                else
                    orientation = getResources().getString(R.string.horizontal);

                rotate.setText(orientation);
            }
        });

        //Init the game

        startGame.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent;
                intent = new Intent(SetupShipsActivity.this, GameShipsActivity.class);
                intent.putExtra("user_list", posShipOnGrid);
                intent.putExtra("ai_list", AIships);

                Bundle args = new Bundle();
                args.putSerializable("battleships", battleShips);
                intent.putExtra("user_battleships", args);

                startActivity(intent);
                overridePendingTransition(R.anim.trans_left_in, R.anim.trans_left_out);
            }
        });
    }

    //Change selected ship and its border color

    private void setBackgroundColor(ImageView iv) {
        for(int i = 0; i < imageShips.size(); i++) {
            if(imageShips.get(i) == iv) {
                imageShips.get(i).setBackground(ContextCompat.getDrawable(getApplicationContext(), R.drawable.border));
                GradientDrawable drawable = (GradientDrawable)imageShips.get(i).getBackground();
                drawable.setStroke(5, ContextCompat.getColor(getApplicationContext(), battleShips.get(i).getColor()));
            }
            else {
                imageShips.get(i).setBackgroundColor(ContextCompat.getColor(getApplicationContext(), R.color.transparent));
            }
        }
    }

    //Manage the Grid and Ship classes
    //Verify if ship can be placed on grid position that user clicked
    //Verify the orientation of the ship
    //Checks if the game can be initiated

    class ImageViewClickListener implements View.OnClickListener {
        private int row = -1;
        private int col = -1;

        public ImageViewClickListener(int row, int column) {
            this.row = row;
            this.col = column;
        }

        public void onClick(View v) {
            if(selectedShip >= 0) {
                ArrayList<String> rowcol = new ArrayList<>();
                rowcol.add(row + "" + col);

                for(int i = 1; i < battleShips.get(selectedShip).getShipSize(); i++) {
                    if(orientation.equals(getResources().getString(R.string.horizontal))) {
                        rowcol.add(row + "" + (col + i));
                    }
                    else {
                        rowcol.add((row + i) + "" + col);
                    }
                }

                boolean canInsert = grid.checkShipOnGrid(selectedShip, rowcol);

                if(canInsert) {
                    ArrayList<String> removeKeys = new ArrayList<>();

                    for (Map.Entry<String, Integer> entry : posShipOnGrid.entrySet()) {
                        String key = entry.getKey();
                        int value = entry.getValue();
                        String[] brokenKey = key.split(",");

                        if(Integer.parseInt(brokenKey[0]) == selectedShip) {
                            findViewById(value).setBackgroundColor(ContextCompat.getColor(getApplicationContext(), R.color.transparent));
                            grid.eraseShipOnGrid(Integer.parseInt(brokenKey[1]));
                            removeKeys.add(key);
                        }
                    }

                    for(int i = 0; i < removeKeys.size(); i++) {
                        posShipOnGrid.remove(removeKeys.get(i));
                    }

                    for (int i = 0; i < rowcol.size(); i++) {
                        posShipOnGrid.put(selectedShip + "," + Integer.parseInt(rowcol.get(i)), imageGrid.get(rowcol.get(i)));
                        grid.saveShipOnGrid(selectedShip, Integer.parseInt(rowcol.get(i)));
                        findViewById(imageGrid.get(rowcol.get(i))).setBackgroundColor(ContextCompat.getColor(getApplicationContext(), battleShips.get(selectedShip).getColor()));
                    }

                    battleShips.get(selectedShip).setOnGrid(true);
                }
                else {
                    Toast toast= Toast.makeText(getApplicationContext(), R.string.error_ship_position, Toast.LENGTH_SHORT);
                    toast.setGravity(Gravity.TOP| Gravity.CENTER_HORIZONTAL, 0, 20);
                    toast.show();
                }

                for(int i = 0; i < battleShips.size(); i++) {
                    if(!battleShips.get(i).isOnGrid())
                        return;
                }

                startGame.setVisibility(View.VISIBLE);
            }
        }
    }

    @Override
    public void onBackPressed ()
    {
        Intent intent;
        intent = new Intent(SetupShipsActivity.this, MainActivity.class);
        startActivity(intent);
        overridePendingTransition(R.anim.trans_right_in, R.anim.trans_right_out);
        finish();
    }
}
