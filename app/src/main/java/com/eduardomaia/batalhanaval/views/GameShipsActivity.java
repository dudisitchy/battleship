package com.eduardomaia.batalhanaval.views;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import com.eduardomaia.batalhanaval.R;
import com.eduardomaia.batalhanaval.objects.AI;
import com.eduardomaia.batalhanaval.objects.Ship;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Eduardo Maia on 23/07/17.
 */

public class GameShipsActivity extends Activity {

    private HashMap<String, Integer> userShips;
    private HashMap<String, Integer> AIships;
    private ArrayList<Ship> battleShips;
    private HashMap<Integer, Integer> userShipsOnGrid;
    private HashMap<String, Integer> imageUserGrid;
    private HashMap<String, Integer> imageAIGrid;
    private ArrayList<Integer> userPlays;
    private ArrayList<Integer> AIPlays;
    private int whoPlays;
    private int totalToWin = 19;
    private int totalFiredPlayer = 0;
    private int totalFiredAI = 0;
    private AI aiPlayer;
    private int countMyShips = 6;
    private int countAIShips = 6;
    private TextView myShips;
    private TextView aiShips;
    private ImageButton exitGame;
    private ImageButton turnSound;
    private MediaPlayer mpExplosion;
    private MediaPlayer mpFire;
    private MediaPlayer mpWater;
    private boolean sound;
    private boolean finishGame;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game_ships);

        //Setup initial arrays

        Intent intent = getIntent();

        userShips = (HashMap<String, Integer>)intent.getSerializableExtra("user_list");

        Bundle args = intent.getBundleExtra("user_battleships");
        battleShips = (ArrayList<Ship>) args.getSerializable("battleships");

        AIships = (HashMap<String, Integer>)intent.getSerializableExtra("ai_list");

        imageUserGrid = new HashMap<>();
        imageAIGrid = new HashMap<>();
        userShipsOnGrid = new HashMap<>();
        userPlays = new ArrayList<>();
        AIPlays = new ArrayList<>();

        //Initial config

        whoPlays = 0;
        aiPlayer = new AI();
        mpExplosion = MediaPlayer.create(this, R.raw.explosion_ship);
        mpFire = MediaPlayer.create(this, R.raw.explosion);
        mpWater = MediaPlayer.create(this, R.raw.splash);
        sound = true;
        finishGame = false;

        myShips = (TextView) findViewById(R.id.countMyShips);
        aiShips = (TextView) findViewById(R.id.countAIShips);

        myShips.setText(String.valueOf(countMyShips));
        aiShips.setText(String.valueOf(countAIShips));

        exitGame = (ImageButton) findViewById(R.id.exit_game);
        turnSound = (ImageButton) findViewById(R.id.turn_sound);

        //Setup grid for user interaction

        TableLayout table = (TableLayout) findViewById(R.id.gridAI);

        for(int row = 0; row < table.getChildCount(); row++) {
            View view = table.getChildAt(row);

            if(view instanceof TableRow) {
                TableRow tableRow = (TableRow) table.getChildAt(row);

                for(int col = 0; col < tableRow.getChildCount(); col++) {
                    if (tableRow.getChildAt(col) instanceof ImageView) {
                        View cellView = tableRow.getChildAt(col);
                        cellView.setOnClickListener(new ImageViewClickListener(row, col));
                        imageUserGrid.put(row + "" + col, cellView.getId());
                    }
                }
            }
        }

        //Setup user's grid for AI play

        TableLayout tableUser = (TableLayout) findViewById(R.id.gridPlayer);

        for(int row = 0; row < tableUser.getChildCount(); row++) {
            View view = tableUser.getChildAt(row);

            if(view instanceof TableRow) {
                TableRow tableRow = (TableRow) tableUser.getChildAt(row);

                for(int col = 0; col < tableRow.getChildCount(); col++) {
                    if (tableRow.getChildAt(col) instanceof ImageView) {
                        View cellView = tableRow.getChildAt(col);
                        imageAIGrid.put(row + "" + col, cellView.getId());

                        for (Map.Entry<String, Integer> entry : userShips.entrySet()) {
                            String key = entry.getKey();

                            String[] posShip = key.split(",");

                            if((row + "" + col).equals(posShip[1])) {
                                userShipsOnGrid.put(Integer.parseInt(row + "" + col), Integer.parseInt(posShip[0]));
                                findViewById(cellView.getId()).setBackgroundColor(ContextCompat.getColor(getApplicationContext(), battleShips.get(Integer.parseInt(posShip[0])).getColor()));
                                break;
                            }
                        }
                    }
                }
            }
        }

        exitGame.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callAlert("exit", "");
            }
        });

        turnSound.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(sound) {
                    sound = false;
                    turnSound.setImageResource(R.mipmap.ic_volume_off_black_24dp);
                }
                else {
                    sound = true;
                    turnSound.setImageResource(R.mipmap.ic_volume_up_black_24dp);
                }
            }
        });
    }

    //Manage the Grid and Ship classes
    //Verify if ship can be placed on grid position that user clicked
    //Verify the orientation of the ship
    //Checks if the game can be initiated

    class ImageViewClickListener implements View.OnClickListener {
        private int row = -1;
        private int col = -1;

        public ImageViewClickListener(int row, int column) {
            this.row = row;
            this.col = column;
        }

        public void onClick(View v) {
            if(whoPlays == 0) {
                int timeWait = 3000;

                for (int i = 0; i < userPlays.size(); i++) {
                    if (userPlays.get(i) == Integer.parseInt(row + "" + col)) {
                        return;
                    }
                }

                userPlays.add(Integer.parseInt(row + "" + col));
                whoPlays = 1;
                boolean fired = false;
                int typeShip = -1;

                for (Map.Entry<String, Integer> entry : AIships.entrySet()) {
                    typeShip = entry.getValue();
                    String value = entry.getKey();

                    if((row + "" + col).equals(value)) {
                        fired = true;
                        break;
                    }
                }

                if(fired) {

                    //Checks if the ship is completely destroyed

                    ArrayList<String> candidates = new ArrayList<>();
                    boolean totalFired = true;

                    for (Map.Entry<String, Integer> entry : AIships.entrySet()) {
                        if(typeShip == entry.getValue()) {
                            boolean exist = false;

                            for (int i = 0; i < userPlays.size(); i++) {
                                if(entry.getKey().equals(String.valueOf(userPlays.get(i)))) {
                                    candidates.add(entry.getKey());
                                    exist = true;
                                }
                            }

                            if(!exist) {
                                totalFired = false;
                                break;
                            }
                        }
                    }

                    if(totalFired) {
                        if(sound)
                            mpExplosion.start();

                        timeWait = 5000;
                        countAIShips--;
                        aiShips.setText(String.valueOf(countAIShips));

                        for (int i = 0; i < candidates.size(); i++) {
                            findViewById(imageUserGrid.get(candidates.get(i))).setBackgroundResource(R.mipmap.fire);
                        }
                    }
                    else {
                        findViewById(imageUserGrid.get(row + "" + col)).setBackgroundResource(R.mipmap.fire);

                        if(sound)
                            mpFire.start();
                    }

                    totalFiredPlayer++;

                    if(totalFiredPlayer == totalToWin) {
                        finishGame("player");
                    }
                }
                else {
                    findViewById(imageUserGrid.get(row + "" + col)).setBackgroundResource(R.mipmap.water_splash);

                    if(sound)
                        mpWater.start();
                }

                new android.os.Handler().postDelayed(
                    new Runnable() {
                        public void run() {
                            if(!finishGame)
                                AITurn();
                        }
                    }, timeWait);
            }
        }
    }
    
    private void AITurn() {
        if(whoPlays == 1) {
            int timeWait = 3000;

            int AIplay = aiPlayer.play(AIPlays);
            AIPlays.add(AIplay);
            boolean fired = false;
            int typeShip = -1;

            for (Map.Entry<Integer, Integer> entry : userShipsOnGrid.entrySet()) {
                int key = entry.getKey();
                typeShip = entry.getValue();

                if (AIplay == key) {
                    fired = true;
                    break;
                }
            }

            if (fired) {

                //Checks if the ship is completely destroyed

                ArrayList<Integer> candidates = new ArrayList<>();
                boolean totalFired = true;

                for (Map.Entry<Integer, Integer> entry : userShipsOnGrid.entrySet()) {
                    if(typeShip == entry.getValue()) {
                        boolean exist = false;

                        for (int i = 0; i < AIPlays.size(); i++) {
                            if(entry.getKey() == AIPlays.get(i)) {
                                candidates.add(entry.getKey());
                                exist = true;
                            }
                        }

                        if(!exist) {
                            totalFired = false;
                            break;
                        }
                    }
                }

                if(totalFired) {
                    if(sound)
                        mpExplosion.start();

                    timeWait = 5000;
                    countMyShips--;
                    myShips.setText(String.valueOf(countMyShips));

                    for (int i = 0; i < candidates.size(); i++) {
                        findViewById(imageAIGrid.get(String.valueOf(candidates.get(i)))).setBackgroundResource(R.mipmap.fire);
                    }
                }
                else {
                    findViewById(imageAIGrid.get(String.valueOf(AIplay))).setBackgroundResource(R.mipmap.fire);

                    if(sound)
                        mpFire.start();
                }

                totalFiredAI++;

                if (totalFiredAI == totalToWin) {
                    finishGame("AI");
                }
            }
            else {
                findViewById(imageAIGrid.get(String.valueOf(AIplay))).setBackgroundResource(R.mipmap.water_splash);

                if(sound)
                    mpWater.start();
            }

            new android.os.Handler().postDelayed(
                new Runnable() {
                    public void run() {
                        if(!finishGame)
                            whoPlays = 0;
                    }
                }, timeWait);
        }
    }

    private void finishGame(String winner) {
        finishGame = true;
        callAlert("winner", winner);
    }

    private void callAlert(final String type, String winner) {
        String title;
        String msg;
        String positive;

        if(type.equals("winner")) {
            positive = getResources().getString(R.string.ok_alert);

            if(winner.equals("player")) {
                title = getResources().getString(R.string.title_win);
                msg = getResources().getString(R.string.msg_win);
            }
            else {
                title = getResources().getString(R.string.title_lose);
                msg = getResources().getString(R.string.msg_lose);
            }
        }
        else {
            title = getResources().getString(R.string.title_alert);
            msg = getResources().getString(R.string.msg_alert);
            positive = getResources().getString(R.string.yes_alert);
        }

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(title);
        builder.setMessage(msg);

        builder.setPositiveButton(positive, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface arg0, int arg1) {
                if(!type.equals("winner")) {
                    Intent intent;
                    intent = new Intent(GameShipsActivity.this, SetupShipsActivity.class);
                    startActivity(intent);
                    overridePendingTransition(R.anim.trans_right_in, R.anim.trans_right_out);
                    finish();
                }
            }
        });

        if(!type.equals("winner")) {
            builder.setNegativeButton(R.string.no_alert, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface arg0, int arg1) {

                }
            });
        }

        AlertDialog alerta = builder.create();
        alerta.show();
    }

    @Override
    public void onBackPressed ()
    {
        callAlert("exit", "");
    }
}
